package com.talentica.bookshelf.leadcapturepage.base;

import android.support.v7.widget.Toolbar;
import com.talentica.bookshelf.leadcapturepage.models.enums.LeadCapturePage;

/**
 * Created by NavalB on 17-05-2016.
 */
interface LeadCapturePageView extends BaseView {

  void setView(int layoutResourceId);

  void setHeader(Toolbar toolbar);

  Toolbar getHeader();

  void setPage(LeadCapturePage leadCapturePage);
}
