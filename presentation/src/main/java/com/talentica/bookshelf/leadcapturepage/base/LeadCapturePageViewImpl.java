package com.talentica.bookshelf.leadcapturepage.base;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import com.talentica.bookshelf.R;
import com.talentica.bookshelf.leadcapturepage.models.enums.LeadCapturePage;

public class LeadCapturePageViewImpl extends AppCompatActivity implements LeadCapturePageView {
  private LeadCapturePagePresenter leadCapturePagePresenter;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    leadCapturePagePresenter = new LeadCapturePagePresenterImpl(this);
    leadCapturePagePresenter.onCreate(savedInstanceState);
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    leadCapturePagePresenter.onSaveInstanceState(outState);
  }

  @Override public void setView(int layoutResourceId) {
    setContentView(layoutResourceId);
  }

  @Override public void setHeader(Toolbar toolbar) {
    setSupportActionBar(toolbar);
  }

  @Override public void setPage(LeadCapturePage leadCapturePage) {
    replaceFragment(leadCapturePage);
  }

/*	@Override
  public Fragment findFragmentByTag(int fragmentTag) {
		return getSupportFragmentManager().findFragmentByTag(getString(fragmentTag));
	}*/

  private void replaceFragment(LeadCapturePage leadCapturePage) {
    Fragment fragment =
        getSupportFragmentManager().findFragmentByTag(getString(leadCapturePage.getFragmentTag()));
    if (fragment == null) {
      fragment = LeadCapturePageFactory.create(leadCapturePage);
    }
    getSupportFragmentManager().beginTransaction()
        .replace(R.id.fragment_container, fragment, getString(leadCapturePage.getFragmentTag()))
        .commit();
  }

  @Override public void onRestoreInstanceState(Bundle savedInstanceState) {
    super.onRestoreInstanceState(savedInstanceState);
    leadCapturePagePresenter.onRestoreSavedInstance(savedInstanceState);
  }

  @Override protected void onStart() {
    super.onStart();
    leadCapturePagePresenter.onStart();
  }

  @Override public Context getContext() {
    return this;
  }

  @Override public void findViews() {
    // no views in activity currently
  }

  @Override public Toolbar getHeader() {
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    if (toolbar == null) {
      throw new IllegalArgumentException("View does not contain toolbar");
    }
    toolbar.setTitle(R.string.title_fragment_home);
    return toolbar;
  }
}

