package com.talentica.bookshelf.leadcapturepage.base;

import android.content.Context;
import android.view.View;

/**
 * Created by NavalB on 17-05-2016.
 */
public interface BaseView {

  Context getContext();

  void findViews();

  View findViewById(int resourceId);
}
