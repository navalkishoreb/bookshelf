package com.talentica.bookshelf.leadcapturepage.home;

import android.view.View;
import android.view.ViewGroup;
import com.talentica.bookshelf.R;

/**
 * Created by NavalB on 28-06-2016.
 */
public class BookFetchingDataCard extends BookCard {
  public static BookCard create(ViewGroup parentView) {
    View view = BookCard.from(R.layout.card_fetching_data,parentView);
    return new BookFetchingDataCard(view);
  }
  public BookFetchingDataCard(View itemView) {
    super(itemView);
  }
}
