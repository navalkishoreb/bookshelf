package com.talentica.bookshelf.leadcapturepage.home;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.talentica.bookshelf.R;
import com.talentica.domain.interactors.UseCase;
import com.talentica.domain.interactors.UseCaseFactory;
import com.talentica.domain.models.Book;
import com.talentica.domain.models.BookListCriteria;

/**
 * Created by NavalB on 31-05-2016.
 */
public class HorizontalBookCarouselView extends RelativeLayout
    implements HorizontalBookCarousel, HorizontalBookCarousel.Listener , HorizontalBookCarouselAdapter.Listener {

  //views
  private RecyclerView carouselBook;
  private TextView carouselTitle;
  private TextView carouselViewAll;

  //data structures
  private Listener listener;
  private BookListCriteria criteria;
  private HorizontalBookCarouselAdapter adapter;
  private UseCase useCase;

  public HorizontalBookCarouselView(Context context) {
    this(context, null);
  }

  public HorizontalBookCarouselView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public HorizontalBookCarouselView(Context context, AttributeSet attrs, int defStyleAttr) {
    this(context, attrs, defStyleAttr, 0);
  }

  @TargetApi(Build.VERSION_CODES.LOLLIPOP)
  public HorizontalBookCarouselView(Context context, AttributeSet attrs, int defStyleAttr,
      int defStyleRes) {
    super(context, attrs, defStyleAttr, defStyleRes);
    init();
  }

  private void init() {
    inflate(getContext(), R.layout.custom_horizontal_book_carousel, this);
    findViews();
    setCarouselViewAll();
    this.adapter = new HorizontalBookCarouselAdapterImpl(this);
    this.carouselBook.setAdapter(adapter);
  }

  @Override public void setCriteria(BookListCriteria criteria) {
    this.criteria = criteria;
    setCarouselTitle();
    this.useCase = UseCaseFactory.get(criteria);
  }

  private void findViews() {
    carouselTitle = (TextView) findViewById(R.id.carousel_title);
    carouselViewAll = (TextView) findViewById(R.id.carousel_view_all);
    carouselBook = (RecyclerView) findViewById(R.id.carousel_recycler_view);
  }

  private void setCarouselTitle() {
    carouselTitle.setText(criteria.getCriteria());
  }

  private void setCarouselViewAll() {
    carouselViewAll.setText(getContext().getString(R.string.carousel_view_all_text));
  }

  @Override public void setListener(Listener listener) {
    this.listener = listener;
  }

  @Override public void onBookClicked(Book book) {
    if (listener != null) {
      listener.onBookClicked(book);
    } else {
      throw new NullPointerException("For now, need presenter to implement this case ");
    }
  }

  @Override public void viewAll(BookListCriteria criteria) {
    if (listener != null) {
      listener.viewAll(criteria);
    } else {
      throw new NullPointerException("For now, need presenter to implement this case ");
    }
  }

  @Override public void loadMore(int currentIndex) {
    new Handler().post(new Runnable() {
      @Override public void run() {
        adapter.exhausted();
      }
    });

  }

}
