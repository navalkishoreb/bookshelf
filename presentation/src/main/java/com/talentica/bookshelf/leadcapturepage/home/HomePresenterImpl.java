package com.talentica.bookshelf.leadcapturepage.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.talentica.bookshelf.R;
import com.talentica.domain.interactors.UseCase;
import com.talentica.domain.interactors.UseCaseFactory;
import com.talentica.domain.models.Book;
import com.talentica.domain.models.BookListCriteria;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by NavalB on 17-05-2016.
 */
public class HomePresenterImpl implements HomePresenter, HorizontalBookCarousel.Listener {

  private final HomeView homeView;
  private final List<BookListCriteria> criteriaList;

  HomePresenterImpl(HomeView homeView) {
    this.homeView = homeView;
    this.criteriaList = getCriteriaList();
  }

  private List<BookListCriteria> getCriteriaList() {
    return Arrays.asList(BookListCriteria.values());
  }

  @Override public void onCreate(Bundle savedInstance) {
  }

  @Override public void onSaveInstanceState(Bundle outState) {
  }

  @Override public void onRestoreSavedInstance(Bundle savedInstanceState) {
  }

  @Override public void onStart() {
  }

  @Override public View setContentView(LayoutInflater inflater, ViewGroup container) {
    ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.fragment_home, container, false);
    for (BookListCriteria criteria : criteriaList) {
      HorizontalBookCarouselView bookCarousel =
          new HorizontalBookCarouselView(homeView.getContext());
      bookCarousel.setListener(this);
      bookCarousel.setCriteria(criteria);
      viewGroup.addView(bookCarousel);
    }
    return viewGroup;
  }

  @Override public void prepare() {
    homeView.findViews();
  }

  @Override public void onBookClicked(Book book) {
  }

  @Override public void viewAll(BookListCriteria criteria) {
  }
}
