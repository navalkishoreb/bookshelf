package com.talentica.bookshelf.leadcapturepage.home;

import android.support.v7.widget.RecyclerView;
import com.talentica.domain.models.Book;
import java.util.List;

/**
 * Created by NavalB on 07-06-2016.
 */
public abstract class HorizontalBookCarouselAdapter extends RecyclerView.Adapter<BookCard> {
  abstract void append(List<Book> books);

  abstract void exhausted();

  interface Listener {
    void loadMore(int currentIndex);
  }
}
