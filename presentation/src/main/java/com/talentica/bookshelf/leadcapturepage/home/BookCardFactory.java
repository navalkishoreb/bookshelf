package com.talentica.bookshelf.leadcapturepage.home;

import android.view.ViewGroup;

/**
 * Created by NavalB on 28-06-2016.
 */
public abstract class BookCardFactory {

  public static BookCard get(ListState listState, ViewGroup parentView) {
    BookCard bookCard;
    switch (listState) {
      case EXHAUSTED:
        bookCard =null;
        break;
      case LOADING:
        bookCard = BookLoadingCard.create(parentView);
        break;
      case NO_DATA_AVAILABLE:
        bookCard = BookNoDataAvailableCard.create(parentView);
        break;
      case FETCHING:
        bookCard = BookFetchingDataCard.create(parentView);
        break;
      case CONTENT:
        bookCard = BookContentCard.create(parentView);
        break;
      default:
        throw new IllegalArgumentException("this list state is not implemented");
    }
    return bookCard;
  }
}
