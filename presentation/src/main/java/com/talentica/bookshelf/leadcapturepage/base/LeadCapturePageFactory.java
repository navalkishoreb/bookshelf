package com.talentica.bookshelf.leadcapturepage.base;

import android.support.v4.app.Fragment;
import com.talentica.bookshelf.leadcapturepage.home.HomeViewImpl;
import com.talentica.bookshelf.leadcapturepage.models.enums.LeadCapturePage;

/**
 * Created by NavalB on 17-05-2016.
 */
abstract class LeadCapturePageFactory {

  static Fragment create(LeadCapturePage leadCapturePage) {
    Fragment fragment;
    switch (leadCapturePage) {
      case HOME:
        fragment = HomeViewImpl.newInstance();
        break;
      case NOTIFICATION:
        fragment = HomeViewImpl.newInstance();
        break;
      case TASKS:
        fragment = HomeViewImpl.newInstance();
        break;
      case PROFILE:
        fragment = HomeViewImpl.newInstance();
        break;
      default:
        throw new IllegalArgumentException(
            leadCapturePage.name() + " initialization not implemented");
    }
    return fragment;
  }
}
