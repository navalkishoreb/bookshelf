package com.talentica.bookshelf.leadcapturepage.home;

import android.view.View;
import android.view.ViewGroup;
import com.talentica.bookshelf.R;

/**
 * Created by NavalB on 28-06-2016.
 */
public class BookNoDataAvailableCard extends BookCard {
  public static BookCard create(ViewGroup parentView) {
    View view = BookCard.from(R.layout.card_no_data_available,parentView);
    return new BookNoDataAvailableCard(view);
  }

  public BookNoDataAvailableCard(View itemView) {
    super(itemView);
  }
}
