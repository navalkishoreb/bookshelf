package com.talentica.bookshelf.leadcapturepage.home;

import android.os.Handler;
import android.view.ViewGroup;
import com.talentica.domain.models.Book;
import java.util.List;
import java.util.Map;

/**
 * Created by NavalB on 07-06-2016.
 */
public class HorizontalBookCarouselAdapterImpl extends HorizontalBookCarouselAdapter {

  private static final int EXTRA_VIEW = 1;
  private HorizontalBookCarouselAdapter.Listener listener;
  private List<Long> bookIdList;
  private Map<Long, Book> bookMap;
  private boolean exhausted;

  HorizontalBookCarouselAdapterImpl(HorizontalBookCarouselAdapter.Listener listener) {
    this.listener = listener;
    this.exhausted = false;
  }

  @Override public void append(List<Book> books) {
  }

  @Override public void exhausted() {
    this.exhausted = true;
    notifyItemChanged(getItemCount());
  }

  @Override public BookCard onCreateViewHolder(ViewGroup parent, int viewType) {
    ListState listState = ListState.from(viewType);
    return BookCardFactory.get(listState, parent);
  }

  @Override public void onBindViewHolder(BookCard holder, int position) {
    if (holder instanceof BookContentCard) {
      ((BookContentCard)holder).setBookDetails(getItem(position));
    }else if(holder instanceof BookFetchingDataCard){
            listener.loadMore(position);
    }
  }

  private Book getItem(int position) {
    if (getSize() != 0) {
      if (bookMap != null) {
        if (bookMap.containsKey(bookIdList.get(position))) {
          return bookMap.get(bookIdList.get(position));
        } else {
          throw new IllegalArgumentException("book map does not contain the key");
        }
      } else {
        throw new NullPointerException("book map is NULL");
      }
    } else {
      throw new NullPointerException("getItem called for wrong View Holder");
    }
  }

  @Override public int getItemCount() {
    return getSize() + EXTRA_VIEW;
  }

  @Override public int getItemViewType(int position) {
    int ordinalValue;
    if (getSize() != 0 && isExhausted() && isLastItem(position)) {
      ordinalValue = ListState.EXHAUSTED.ordinal();
    } else if (getSize() != 0 && !isExhausted() && isLastItem(position)) {
      ordinalValue = ListState.LOADING.ordinal();
    } else if (getSize() == 0 && !isExhausted()) {
      ordinalValue = ListState.FETCHING.ordinal();
    } else if (getSize() == 0 && isExhausted()) {
      ordinalValue = ListState.NO_DATA_AVAILABLE.ordinal();
    } else if (getSize() != 0 && !isLastItem(position)) {
      ordinalValue = ListState.CONTENT.ordinal();
    } else {
      throw new IllegalArgumentException("this condition is not met");
    }

    return ordinalValue;
  }

  int getSize() {
    if (bookIdList != null) {
      return bookIdList.size();
    }
    return 0;
  }

  public boolean isExhausted() {
    return exhausted;
  }

  public boolean isLastItem(int position) {
    return position == getItemCount();
  }
}

