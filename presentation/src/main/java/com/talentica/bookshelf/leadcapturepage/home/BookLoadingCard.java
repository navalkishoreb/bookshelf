package com.talentica.bookshelf.leadcapturepage.home;

import android.view.View;
import android.view.ViewGroup;
import com.talentica.bookshelf.R;

/**
 * Created by NavalB on 28-06-2016.
 */
public class BookLoadingCard extends BookCard {
  public static BookCard create(ViewGroup parentView) {
    View view = BookCard.from(R.layout.card_loading, parentView);
    return new BookLoadingCard(view);
  }

  public BookLoadingCard(View itemView) {
    super(itemView);
  }
}
