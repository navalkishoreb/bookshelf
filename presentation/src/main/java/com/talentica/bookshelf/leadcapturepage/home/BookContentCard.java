package com.talentica.bookshelf.leadcapturepage.home;

import android.view.View;
import android.view.ViewGroup;
import com.talentica.bookshelf.R;
import com.talentica.domain.models.Book;

/**
 * Created by NavalB on 28-06-2016.
 */
public class BookContentCard extends BookCard {
  private Book book;

  public static BookCard create(ViewGroup parentView) {
    View view = BookCard.from(R.layout.card_book_content, parentView);
    return new BookContentCard(view);
  }

  public BookContentCard(View itemView) {
    super(itemView);
  }

  public void setBookDetails(Book book) {
    this.book = book;
  }
}
