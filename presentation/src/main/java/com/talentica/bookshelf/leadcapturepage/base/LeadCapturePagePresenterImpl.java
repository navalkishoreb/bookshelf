package com.talentica.bookshelf.leadcapturepage.base;

import android.os.Bundle;
import com.talentica.bookshelf.R;
import com.talentica.bookshelf.leadcapturepage.models.enums.LeadCapturePage;

/**
 * Created by NavalB on 17-05-2016.
 */
class LeadCapturePagePresenterImpl implements LeadCapturePagePresenter {

  private final static String KEY_CURRENT_PAGE = "leadCapturePage";
  private final LeadCapturePageView leadCapturePageView;
  private LeadCapturePage currentPage = LeadCapturePage.HOME;

  LeadCapturePagePresenterImpl(LeadCapturePageView leadCapturePageView) {
    this.leadCapturePageView = leadCapturePageView;
  }

  @Override public void onCreate(Bundle savedInstance) {
    leadCapturePageView.setView(R.layout.activity_lead_capture_page);
    leadCapturePageView.setHeader(leadCapturePageView.getHeader());
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    outState.putInt(KEY_CURRENT_PAGE, currentPage.ordinal());
  }

  @Override public void onRestoreSavedInstance(Bundle savedInstanceState) {
    currentPage = LeadCapturePage.values()[savedInstanceState.getInt(KEY_CURRENT_PAGE)];
  }

  @Override public void onStart() {
    leadCapturePageView.setPage(currentPage);
  }



  @Override public void changePageTo(LeadCapturePage leadCapturePage) {
    this.currentPage = leadCapturePage;
    leadCapturePageView.setPage(currentPage);
  }
}
