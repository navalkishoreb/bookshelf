package com.talentica.bookshelf.leadcapturepage.home;

/**
 * Created by NavalB on 28-06-2016.
 */
public enum ListState {
  NO_DATA_AVAILABLE("No Data Available!"),
  FETCHING("Fetching data..."),
  CONTENT("Ideal View"),
  LOADING("Loading View"),
  EXHAUSTED("Exhausted View");

  private final String value;

  ListState(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }

  public static ListState from(int ordinal) {
    switch (ordinal) {
      case 0:
        return NO_DATA_AVAILABLE;
      case 1:
        return FETCHING;
      case 2:
        return CONTENT;
      case 3:
        return LOADING;
      case 4:
        return EXHAUSTED;
      default:
        throw new IllegalArgumentException("No such list state exists");
    }
  }
}
