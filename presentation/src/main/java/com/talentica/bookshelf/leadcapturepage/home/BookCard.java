package com.talentica.bookshelf.leadcapturepage.home;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by NavalB on 28-06-2016.
 */
public abstract class BookCard extends RecyclerView.ViewHolder {
  public BookCard(View itemView) {
    super(itemView);
  }

  public static View from(int layoutId, ViewGroup parentView) {
    return LayoutInflater.from(parentView.getContext()).inflate(layoutId, parentView, false);
  }
}
