package com.talentica.bookshelf.leadcapturepage.home;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.talentica.bookshelf.leadcapturepage.base.BasePresenter;

/**
 * Created by NavalB on 17-05-2016.
 */
interface HomePresenter extends BasePresenter {
  View setContentView(LayoutInflater inflater, ViewGroup container);

  void prepare();
}
