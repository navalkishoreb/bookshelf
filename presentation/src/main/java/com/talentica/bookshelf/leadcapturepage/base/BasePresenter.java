package com.talentica.bookshelf.leadcapturepage.base;

import android.os.Bundle;

/**
 * Created by NavalB on 17-05-2016.
 */
public interface BasePresenter {
  void onCreate(Bundle savedInstance);

  void onSaveInstanceState(Bundle outState);

  void onRestoreSavedInstance(Bundle savedInstanceState);

  void onStart();
}
