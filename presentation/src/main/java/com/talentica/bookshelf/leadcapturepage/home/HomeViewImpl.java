package com.talentica.bookshelf.leadcapturepage.home;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeViewImpl extends Fragment implements HomeView {

  private HomePresenter homePresenter;

  public HomeViewImpl() {
    // Required empty public constructor
  }

  public static Fragment newInstance() {
    return new HomeViewImpl();
  }

  @Override public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    homePresenter = new HomePresenterImpl(this);
    homePresenter.onCreate(savedInstanceState);
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    return homePresenter.setContentView(inflater, container);
  }

  @Override public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    homePresenter.prepare();
  }

  @Override public void findViews() {
  }

  @Override public void onResume() {
    super.onResume();
  }

  @Override public void onPause() {
    super.onPause();
  }

  @Override public View findViewById(int resourceId) {
    return getView().findViewById(resourceId);
  }

  @NonNull @Override public View getView() {
    View view = super.getView();
    if (view == null) {
      throw new NullPointerException("RootView is NULL");
    }else {
      return view;
    }
  }
}
