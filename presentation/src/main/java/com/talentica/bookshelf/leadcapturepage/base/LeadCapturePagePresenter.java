package com.talentica.bookshelf.leadcapturepage.base;

import com.talentica.bookshelf.leadcapturepage.models.enums.LeadCapturePage;

/**
 * Created by NavalB on 17-05-2016.
 */
public interface LeadCapturePagePresenter extends BasePresenter {

  void changePageTo(LeadCapturePage leadCapturePage);
}
