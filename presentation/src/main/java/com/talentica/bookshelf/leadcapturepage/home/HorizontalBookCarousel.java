package com.talentica.bookshelf.leadcapturepage.home;

import com.talentica.domain.models.Book;
import com.talentica.domain.models.BookListCriteria;

/**
 * Created by NavalB on 31-05-2016.
 */
public interface HorizontalBookCarousel {

  void setListener(Listener listener);
  void setCriteria(BookListCriteria criteria);

  interface Listener {

    void onBookClicked(Book book);

    void viewAll(BookListCriteria criteria);

  }
}
