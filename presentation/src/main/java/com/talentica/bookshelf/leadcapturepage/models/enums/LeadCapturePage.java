package com.talentica.bookshelf.leadcapturepage.models.enums;

import com.talentica.bookshelf.R;

/**
 * Created by NavalB on 17-05-2016.
 */
public enum LeadCapturePage {
  HOME(R.string.tag_fragment_home, R.string.title_fragment_home, R.mipmap.ic_launcher),
  PROFILE(R.string.tag_fragment_profile, R.string.title_fragment_profile, R.mipmap.ic_launcher),
  NOTIFICATION(R.string.tag_fragment_notifications, R.string.title_fragment_notifications,
      R.mipmap.ic_launcher),
  TASKS(R.string.tag_fragment_tasks, R.string.title_fragment_tasks, R.mipmap.ic_launcher);

  private int fragmentTag;
  private int title;
  private int icon;

  LeadCapturePage(int fragmentTag, int title, int icon) {
    this.fragmentTag = fragmentTag;
    this.title = title;
    this.icon = icon;
  }

  public int getFragmentTag() {
    return fragmentTag;
  }

  public int getTitle() {
    return title;
  }

  public int getIcon() {
    return icon;
  }
}
