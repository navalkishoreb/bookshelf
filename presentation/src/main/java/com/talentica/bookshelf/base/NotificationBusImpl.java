package com.talentica.bookshelf.base;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by NavalB on 29-06-2016.
 */
public class NotificationBusImpl implements NotificationBus {

  private static Set<Object> observer;

  NotificationBusImpl() {
    observer = new HashSet<>();
  }

  public static void register(Object object){
    observer.add(object);
  }

  public static void remove(Object object){
    observer.remove(object);
  }
}
