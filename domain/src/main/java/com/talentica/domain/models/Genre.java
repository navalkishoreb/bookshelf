package com.talentica.domain.models;

/**
 * Created by NavalB on 31-05-2016.
 */
public enum Genre {
	SCIENCE_FICTION("Science Fiction"),
	SATIRE("Satire"),
	DRAMA("Drama"),
	ACTION_AND_ADEVENTURE("Action and Adventure"),
	ROMANCE("Romance"),
	MYSTERY("Mystery"),
	HORROR("Horror"),
	SELF_HELP("Self Help"),
	HEALTH("Health"),
	GUIDE("Guide"),
	TRAVEL("Travel"),
	CHILDERN("Children\'s"),
	RELIGION_SPIRITUAL("Religion, Spirituality & New Age"),
	SCIENCE("Scince"),
	HISTORY("History");

	private final String genre;

	Genre(String genre) {this.genre = genre;}

	public String getGenre() {
		return genre;
	}
}
