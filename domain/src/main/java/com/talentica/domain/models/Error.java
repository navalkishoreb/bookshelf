package com.talentica.domain.models;

/**
 * Created by NavalB on 31-05-2016.
 */
public interface Error {

	String getMessage();

	int getErrorCode();

}
