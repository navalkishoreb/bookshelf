package com.talentica.domain.models;

/**
 * Created by NavalB on 31-05-2016.
 */
public enum BookListCriteria {
	RECENTLY_ADDED("Recently Added"),
	MOST_READ("Most Read"),
	TOP_RATED("Top Rated");


	private final String category;

	BookListCriteria(String category) {
		this.category = category;
	}

	public String getCriteria() {
		return category;
	}
}
