package com.talentica.domain.interactors;

import com.talentica.domain.models.BookListCriteria;

/**
 * Created by NavalB on 31-05-2016.
 */
public abstract class UseCaseFactory {

	public static UseCase get(BookListCriteria criteria) {
		UseCase useCase;
		switch (criteria) {
			case RECENTLY_ADDED:
				useCase = new RecentlyAdded();
				break;
			case MOST_READ:
				useCase = new MostRead();
				break;
			case TOP_RATED:
				useCase =  new RecentlyAdded();
				break;
			default:
				throw new NullPointerException(criteria.name()+" use case has not been implemented");
		}
		return useCase;
	}
}
