/*
package com.talentica.domain.interactors;

import com.talentica.domain.base.Debug;
import com.talentica.domain.models.BookListCriteria;

*/
/**
 * Created by NavalB on 31-05-2016.
 *//*

public class FetchHomePageList implements UseCase {
	private static final String TAG = FetchHomePageList.class.getSimpleName();

	private final UseCase useCase;
	private final BookListCriteria criteria;

	public FetchHomePageList(BookListCriteria criteria) {
		this.useCase = UseCaseFactory.get(criteria);
		this.criteria = criteria;
	}

	@Override
	public void execute() {
		if (useCase != null) {
			useCase.execute();
		} else {
			Debug.e(TAG, String.format("Use case for %s is not available.", criteria.getCriteria()));
		}
	}
}


*/
