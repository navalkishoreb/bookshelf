package com.talentica.domain.interactors;

		import com.talentica.domain.models.Error;

/**
 * Created by NavalB on 31-05-2016.
 */
public interface UseCase<T> {

	public interface Listener<T>{
		void onSuccess(T object);
		void onError(Error error);
	}

	void execute(Listener<T> listener);
}
