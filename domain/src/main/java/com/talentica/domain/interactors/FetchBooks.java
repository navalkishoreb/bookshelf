package com.talentica.domain.interactors;

import android.os.Handler;
import android.os.Looper;

import com.talentica.data.BookRepository;
import com.talentica.data.BookRepositoryImpl;
import com.talentica.domain.models.Book;
import com.talentica.domain.models.Error;
import java.util.List;

/**
 * Created by NavalB on 07-06-2016.
 */
public  abstract class FetchBooks implements UseCase<List<Book>>, UseCase.Listener{

	private BookRepository bookRepository;
	private Handler uiHandler;
  private Listener<List<Book>> listener;

	FetchBooks() {
		this.bookRepository = new BookRepositoryImpl();
		this.uiHandler = new Handler(Looper.getMainLooper());
	}

  @Override public void execute(Listener<List<Book>> listener) {
    this.listener = listener;
  }

  @Override public void onSuccess(Object object) {

  }

  @Override public void onError(Error error) {

  }
}
