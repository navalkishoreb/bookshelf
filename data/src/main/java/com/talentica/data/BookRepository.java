package com.talentica.data;

/**
 * Created by NavalB on 31-05-2016.
 */
public interface BookRepository {

	void fetchRecentlyAddedBooks();

	void fetchMostReadBooks();

  void fetch();
}
